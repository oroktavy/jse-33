package ru.aushakov.tm.server.util;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

public interface SystemUtil {

    static long getPID() {
        @Nullable final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (!StringUtils.isEmpty(processName)) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            } catch (final Exception e) {
                return 0;
            }
        }
        return 0;
    }

}
