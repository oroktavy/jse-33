package ru.aushakov.tm.server.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {

    PLANNED("Planned"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    @NotNull
    private static final Status[] staticValues = values();

    Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @Nullable
    public static Status toStatus(@Nullable final String statusId) {
        for (@NotNull final Status status : staticValues) {
            if (status.name().equalsIgnoreCase(statusId)) return status;
        }
        return null;
    }

}
