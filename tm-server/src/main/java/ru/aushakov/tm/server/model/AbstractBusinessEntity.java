package ru.aushakov.tm.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.api.entity.IWBS;
import ru.aushakov.tm.server.enumerated.Status;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @Nullable
    protected String name;

    @Nullable
    protected String description;

    @NotNull
    protected Status status = Status.PLANNED;

    @Nullable
    protected Date startDate;

    @Nullable
    protected Date endDate;

    @Nullable
    protected String userId;

    protected AbstractBusinessEntity(@NotNull @JsonProperty("name") final String name) {
        this.name = name;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
        switch (status) {
            case IN_PROGRESS:
                setStartDate(new Date());
                break;
            case COMPLETED:
                setEndDate(new Date());
                break;
        }
    }

    @Override
    @NotNull
    public String toString() {
        return id + ": " + name;
    }

}
