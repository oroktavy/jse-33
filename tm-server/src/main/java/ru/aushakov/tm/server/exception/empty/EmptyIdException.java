package ru.aushakov.tm.server.exception.empty;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Provided id is empty!");
    }

}
