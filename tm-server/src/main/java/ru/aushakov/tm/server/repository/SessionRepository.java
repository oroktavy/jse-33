package ru.aushakov.tm.server.repository;

import ru.aushakov.tm.server.api.repository.ISessionRepository;
import ru.aushakov.tm.server.model.Session;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
}
