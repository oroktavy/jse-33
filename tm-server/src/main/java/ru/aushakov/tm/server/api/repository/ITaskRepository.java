package ru.aushakov.tm.server.api.repository;

import ru.aushakov.tm.server.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    Task assignTaskToProject(String taskId, String projectId, String userId);

    Task unbindTaskFromProject(String taskId, String userId);

    List<Task> findAllTasksByProjectId(String projectId, String userId);

    List<Task> removeAllTasksByProjectId(String projectId, String userId);

}
