package ru.aushakov.tm.server.exception.empty;

public class EmptySessionException extends RuntimeException {

    public EmptySessionException() {
        super("No session provided!");
    }

}
