package ru.aushakov.tm.server.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.util.HashUtil;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity {

    @Nullable
    private String userId;

    @Nullable
    private String signature;

    public Session(@NotNull final String userId) {
        this.userId = userId;
    }

    @SneakyThrows
    public void sign(@NotNull final String secret, @NotNull final Integer iterationNum) {
        this.setSignature(null);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        @NotNull final String signingResult = HashUtil.salt(json, secret, iterationNum);
        this.setSignature(signingResult);
    }

}
