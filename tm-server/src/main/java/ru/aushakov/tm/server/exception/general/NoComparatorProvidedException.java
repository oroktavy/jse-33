package ru.aushakov.tm.server.exception.general;

public class NoComparatorProvidedException extends RuntimeException {

    public NoComparatorProvidedException() {
        super("No comparator is provided for sort!");
    }

}
