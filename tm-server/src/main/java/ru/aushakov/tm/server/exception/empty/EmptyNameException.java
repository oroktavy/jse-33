package ru.aushakov.tm.server.exception.empty;

public class EmptyNameException extends RuntimeException {

    public EmptyNameException() {
        super("Provided name is empty!");
    }

}
