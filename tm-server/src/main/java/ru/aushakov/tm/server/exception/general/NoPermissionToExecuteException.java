package ru.aushakov.tm.server.exception.general;

public class NoPermissionToExecuteException extends RuntimeException {

    public NoPermissionToExecuteException() {
        super("You don't have permissions to execute this command!");
    }

}
