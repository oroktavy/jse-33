package ru.aushakov.tm.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.dto.adapter.UserAdapter;
import ru.aushakov.tm.server.enumerated.Role;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Setter
@Getter
@XmlJavaTypeAdapter(UserAdapter.class)
public final class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String lastName;

    @Nullable
    private String firstName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    private boolean lockedFlag = false;

    public User(
            @NotNull @JsonProperty("login") String login,
            @NotNull @JsonProperty("passwordHash") String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

}
