package ru.aushakov.tm.server.dto.adapter;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.server.model.User;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class UserAdapter extends XmlAdapter<AdaptedUser, User> {

    @Override
    @SneakyThrows
    public User unmarshal(@NotNull final AdaptedUser adaptedUser) {
        @NotNull final User user = new User(adaptedUser.getLogin(), adaptedUser.getPasswordHash());
        user.setId(adaptedUser.getId());
        user.setCreated(adaptedUser.getCreated());
        user.setEmail(adaptedUser.getEmail());
        user.setLastName(adaptedUser.getLastName());
        user.setFirstName(adaptedUser.getFirstName());
        user.setMiddleName(adaptedUser.getMiddleName());
        user.setRole(adaptedUser.getRole());
        user.setLockedFlag(adaptedUser.isLockedFlag());
        return user;
    }

    @Override
    @SneakyThrows
    public AdaptedUser marshal(@NotNull final User user) {
        @NotNull final AdaptedUser adaptedUser = new AdaptedUser();
        adaptedUser.setLogin(user.getLogin());
        adaptedUser.setPasswordHash(user.getPasswordHash());
        adaptedUser.setId(user.getId());
        adaptedUser.setCreated(user.getCreated());
        adaptedUser.setEmail(user.getEmail());
        adaptedUser.setLastName(user.getLastName());
        adaptedUser.setFirstName(user.getFirstName());
        adaptedUser.setMiddleName(user.getMiddleName());
        adaptedUser.setRole(user.getRole());
        adaptedUser.setLockedFlag(user.isLockedFlag());
        return adaptedUser;
    }

}
