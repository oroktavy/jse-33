package ru.aushakov.tm.server.exception.general;

public class WrongCredentialsException extends RuntimeException {

    public WrongCredentialsException() {
        super("Wrong credentials, login failed!");
    }

}
