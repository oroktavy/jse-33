package ru.aushakov.tm.server.api.service;

import ru.aushakov.tm.server.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    Project deepDeleteProjectById(String projectId, String userId);

}
