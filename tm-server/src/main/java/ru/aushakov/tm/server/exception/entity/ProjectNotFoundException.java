package ru.aushakov.tm.server.exception.entity;

public class ProjectNotFoundException extends RuntimeException {

    public ProjectNotFoundException() {
        super("Project not found!");
    }

}
