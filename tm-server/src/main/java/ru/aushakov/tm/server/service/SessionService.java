package ru.aushakov.tm.server.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.api.repository.ISessionRepository;
import ru.aushakov.tm.server.api.repository.IUserRepository;
import ru.aushakov.tm.server.api.service.IPropertyService;
import ru.aushakov.tm.server.api.service.ISessionService;
import ru.aushakov.tm.server.enumerated.ConfigProperty;
import ru.aushakov.tm.server.enumerated.Role;
import ru.aushakov.tm.server.exception.empty.EmptyLoginException;
import ru.aushakov.tm.server.exception.empty.EmptyPasswordException;
import ru.aushakov.tm.server.exception.empty.EmptySessionException;
import ru.aushakov.tm.server.exception.general.WrongCredentialsException;
import ru.aushakov.tm.server.model.Session;
import ru.aushakov.tm.server.model.User;
import ru.aushakov.tm.server.util.HashUtil;

import java.util.Optional;

public final class SessionService implements ISessionService {

    @NotNull
    private final String sesSecret;

    @NotNull
    private final Integer sesIterationNum;

    @NotNull
    private final String pwSecret;

    @NotNull
    private final Integer pwIterationNum;

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IUserRepository userRepository;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IUserRepository userRepository,
            @NotNull final IPropertyService propertyService
    ) {
        this.sesSecret = propertyService.getProperty(ConfigProperty.SESSION_SECRET);
        this.sesIterationNum = propertyService.getIntProperty(ConfigProperty.SESSION_ITERATION);
        this.pwSecret = propertyService.getProperty(ConfigProperty.PASSWORD_SECRET);
        this.pwIterationNum = propertyService.getIntProperty(ConfigProperty.PASSWORD_ITERATION);
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
    }

    @Override
    @NotNull
    public Session open(@Nullable final String login, @Nullable final String password) {
        @NotNull final String userId = login(login, password);
        @NotNull final Session session = new Session(userId);
        session.sign(sesSecret, sesIterationNum);
        sessionRepository.add(session);
        return session;
    }

    @Override
    public void close(@Nullable final Session session) {
        Optional.ofNullable(session).orElseThrow(EmptySessionException::new);
        sessionRepository.removeOneById(session.getId());
    }

    @Override
    public boolean validate(@Nullable final Session session) {
        if (session == null) return false;
        @Nullable final String signature = session.getSignature();
        @Nullable final String userId = session.getUserId();
        if (StringUtils.isEmpty(signature) || StringUtils.isEmpty(userId)) return false;
        session.sign(sesSecret, sesIterationNum);
        if (!signature.equals(session.getSignature())) return false;
        @Nullable final Session repoSession = sessionRepository.findOneById(session.getId());
        return (repoSession != null
                && signature.equals(repoSession.getSignature())
                && session.getCreated().getTime() == repoSession.getCreated().getTime()
                && userId.equals(repoSession.getUserId()));
    }

    @Override
    public boolean validateAdmin(@Nullable final Session session) {
        if (session == null) return false;
        @Nullable final String userId = session.getUserId();
        if (StringUtils.isEmpty(userId)) return false;
        @Nullable final User user = userRepository.findOneById(userId);
        if (user == null || user.getRole() != Role.ADMIN) return false;
        return validate(session);
    }

    @Override
    @NotNull
    public String login(@Nullable final String login, @Nullable final String password) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        if (StringUtils.isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final User user = userRepository.findOneByLogin(login);
        if (
                user == null
                        || !HashUtil.salt(password, pwSecret, pwIterationNum).equals(user.getPasswordHash())
                        || user.isLockedFlag()
        ) {
            throw new WrongCredentialsException();
        }
        return user.getId();
    }

    @Override
    public void changePassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final String userId
    ) {
        if (StringUtils.isEmpty(userId)) throw new WrongCredentialsException();
        if (StringUtils.isEmpty(oldPassword) || StringUtils.isEmpty(newPassword)) {
            throw new EmptyPasswordException();
        }
        @Nullable final User user = userRepository.findOneById(userId);
        if (user == null) throw new WrongCredentialsException();
        @NotNull final String oldPasswordHash = user.getPasswordHash();
        if (!oldPasswordHash.equals(HashUtil.salt(oldPassword, pwSecret, pwIterationNum)))
            throw new WrongCredentialsException();
        userRepository.setPassword(user.getLogin(), HashUtil.salt(newPassword, pwSecret, pwIterationNum));
    }

    @Override
    public void closeAllSessions() {
        sessionRepository.clear();
    }

}
