package ru.aushakov.tm.server.api.service;

import ru.aushakov.tm.server.enumerated.Role;
import ru.aushakov.tm.server.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    boolean isUserAuthenticated();

    String getUserId();

    String getUserLogin();

    User getUser();

    void changePassword(String oldPassword, String newPassword);

    void checkRoles(Role[] roles);

}
