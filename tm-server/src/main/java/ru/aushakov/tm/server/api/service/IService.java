package ru.aushakov.tm.server.api.service;

import ru.aushakov.tm.server.api.repository.IRepository;
import ru.aushakov.tm.server.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
