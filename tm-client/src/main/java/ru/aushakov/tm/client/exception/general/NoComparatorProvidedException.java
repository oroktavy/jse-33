package ru.aushakov.tm.client.exception.general;

public class NoComparatorProvidedException extends RuntimeException {

    public NoComparatorProvidedException() {
        super("No comparator is provided for sort!");
    }

}
