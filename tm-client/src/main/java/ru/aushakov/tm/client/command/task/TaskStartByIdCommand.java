package ru.aushakov.tm.client.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractTaskCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Optional;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.TASK_START_BY_ID;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Start task by id";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        System.out.println("[START TASK]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getTaskService().getTaskEndpointPort().startTaskById(id, session))
                .orElseThrow(TaskNotFoundException::new);
    }

}
