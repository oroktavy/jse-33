package ru.aushakov.tm.client.command.data;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.api.service.IAuthService;
import ru.aushakov.tm.client.command.AbstractDataCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.AdminEndpoint;
import ru.aushakov.tm.client.endpoint.Domain;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.ConfigProperty;
import ru.aushakov.tm.client.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataLoadFromXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_LOAD_FROM_XML;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load application data from xml format";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FROM XML]");
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final Session session = authService.getSession();
        @NotNull final AdminEndpoint dataService = serviceLocator.getDataService().getAdminEndpointPort();
        @NotNull final String fileXml = dataService.getDataFileName(ConfigProperty.DATAFILE_XML.name(), session);
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(fileXml)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final AnnotationIntrospector annotationIntrospector =
                new JaxbAnnotationIntrospector(objectMapper.getTypeFactory());
        objectMapper.setAnnotationIntrospector(annotationIntrospector);
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        dataService.setDomain(domain, session);
        authService.setSession(null);
    }

}
