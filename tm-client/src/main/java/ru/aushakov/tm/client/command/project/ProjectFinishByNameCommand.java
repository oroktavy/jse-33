package ru.aushakov.tm.client.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractProjectCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Optional;

public final class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.PROJECT_FINISH_BY_NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Finish project by name";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getProjectService()
                .getProjectEndpointPort().finishProjectByName(name, session))
                .orElseThrow(ProjectNotFoundException::new);
    }

}
