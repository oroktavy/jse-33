package ru.aushakov.tm.client.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractCommand;
import ru.aushakov.tm.client.constant.TerminalConst;

public final class ExitCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_EXIT;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
