package ru.aushakov.tm.client.exception.empty;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Provided id is empty!");
    }

}
