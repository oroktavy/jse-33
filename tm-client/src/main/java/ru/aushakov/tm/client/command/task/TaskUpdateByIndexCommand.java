package ru.aushakov.tm.client.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractTaskCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Optional;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.TASK_UPDATE_BY_INDEX;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update task by index";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getTaskService()
                .getTaskEndpointPort().updateTaskByIndex(index, session, name, description))
                .orElseThrow(TaskNotFoundException::new);
    }

}
