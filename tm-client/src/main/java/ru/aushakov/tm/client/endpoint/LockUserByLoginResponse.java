
package ru.aushakov.tm.client.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for lockUserByLoginResponse complex type.
 * <p>
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * <p>
 * &lt;pre&gt;
 * &amp;lt;complexType name="lockUserByLoginResponse"&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="return" type="{http://endpoint.server.tm.aushakov.ru/}adaptedUser" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lockUserByLoginResponse", propOrder = {
        "_return"
})
public class LockUserByLoginResponse {

    @XmlElement(name = "return")
    protected AdaptedUser _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link AdaptedUser }
     */
    public AdaptedUser getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link AdaptedUser }
     */
    public void setReturn(AdaptedUser value) {
        this._return = value;
    }

}
