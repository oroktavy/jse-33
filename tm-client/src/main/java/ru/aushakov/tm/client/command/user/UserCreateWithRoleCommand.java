package ru.aushakov.tm.client.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractUserCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Arrays;

public final class UserCreateWithRoleCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.USER_CREATE_WITH_ROLE;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create user with role";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER LOGIN:");
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL (OPTIONAL):");
        @Nullable final String email = TerminalUtil.nextLine();
        System.out.println("ENTER ROLE " + Arrays.toString(Role.values()) + " :");
        @Nullable final String roleId = TerminalUtil.nextLine();
        serviceLocator.getDataService().getAdminEndpointPort().addUserWithRole(login, password, email, roleId, session);
    }

}
