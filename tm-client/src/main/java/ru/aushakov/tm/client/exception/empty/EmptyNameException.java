package ru.aushakov.tm.client.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.client.command.AbstractCommand;

public class EmptyNameException extends RuntimeException {

    public EmptyNameException() {
        super("Provided name is empty!");
    }

    public EmptyNameException(@NotNull final AbstractCommand command) {
        super("Provided command '" + command.getClass().getName() + "' has empty name!");
    }

}
