package ru.aushakov.tm.client.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.client.command.AbstractCommand;
import ru.aushakov.tm.client.constant.ArgumentConst;
import ru.aushakov.tm.client.constant.TerminalConst;

public final class VersionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_VERSION;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ArgumentConst.ARG_VERSION;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show application version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("version") + "." + Manifests.read("buildNumber"));
    }

}
