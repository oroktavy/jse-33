package ru.aushakov.tm.client.command;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.api.ServiceLocator;
import ru.aushakov.tm.client.enumerated.Role;

public abstract class AbstractCommand {

    @NonNull
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public Role[] getRoles() {
        return null;
    }

    public abstract void execute();

    @Override
    @NotNull
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String arg = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (!StringUtils.isEmpty(name)) result += name;
        if (!StringUtils.isEmpty(arg)) result += ": [" + arg + "]";
        if (!StringUtils.isEmpty(description)) result += " - " + description;
        return result;
    }

}
