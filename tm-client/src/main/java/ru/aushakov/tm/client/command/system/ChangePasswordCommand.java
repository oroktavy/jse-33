package ru.aushakov.tm.client.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.api.service.IAuthService;
import ru.aushakov.tm.client.command.AbstractCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.endpoint.UserEndpoint;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.util.TerminalUtil;

public final class ChangePasswordCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_CHANGE_PASSWORD;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change your current password";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final Session session = authService.getSession();
        @Nullable final String oldPassword = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        @NotNull final UserEndpoint userService = serviceLocator.getUserService().getUserEndpointPort();
        userService.changePassword(oldPassword, newPassword, session);
        authService.setSession(null);
    }

}
