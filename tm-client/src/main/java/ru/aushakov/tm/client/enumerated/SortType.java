package ru.aushakov.tm.client.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum SortType {

    NAME("Sort by name"),
    STATUS("Sort by status"),
    CREATED("Sort by created date"),
    START_DATE("Sort by start date"),
    END_DATE("Sort by end date");

    @NotNull
    private final String displayName;

    @NotNull
    private static final SortType[] staticValues = values();

    SortType(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @Nullable
    public static SortType toSortType(@Nullable final String sortTypeId) {
        for (@NotNull final SortType sortType : staticValues) {
            if (sortType.name().equalsIgnoreCase(sortTypeId)) return sortType;
        }
        return null;
    }

}
