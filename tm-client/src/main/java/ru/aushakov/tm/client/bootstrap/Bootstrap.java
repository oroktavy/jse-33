package ru.aushakov.tm.client.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.api.ServiceLocator;
import ru.aushakov.tm.client.api.repository.ICommandRepository;
import ru.aushakov.tm.client.api.service.IAuthService;
import ru.aushakov.tm.client.api.service.ICommandService;
import ru.aushakov.tm.client.api.service.ILoggerService;
import ru.aushakov.tm.client.api.service.IPropertyService;
import ru.aushakov.tm.client.command.AbstractCommand;
import ru.aushakov.tm.client.component.FileScanner;
import ru.aushakov.tm.client.endpoint.*;
import ru.aushakov.tm.client.exception.general.UnknownArgumentException;
import ru.aushakov.tm.client.exception.general.UnknownCommandException;
import ru.aushakov.tm.client.repository.CommandRepository;
import ru.aushakov.tm.client.service.AuthService;
import ru.aushakov.tm.client.service.CommandService;
import ru.aushakov.tm.client.service.LoggerService;
import ru.aushakov.tm.client.service.PropertyService;
import ru.aushakov.tm.client.util.SystemUtil;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthService authService = new AuthService(propertyService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @SneakyThrows
    private void createPIDFile() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run(@Nullable final String[] args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        commandService.initCommands(this);
        createPIDFile();
        fileScanner.init();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseArg(@Nullable final String arg) {
        @NotNull final AbstractCommand command = Optional
                .ofNullable(commandService.getCommandByArg(arg))
                .orElseThrow(() -> new UnknownArgumentException(arg));
        command.execute();
    }

    public void parseCommand(@Nullable final String command) {
        @NotNull final AbstractCommand parsedCommand = Optional
                .ofNullable(commandService.getCommandByName(command))
                .orElseThrow(() -> new UnknownCommandException(command));
        parsedCommand.execute();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        if (args.length > 1) System.out.println("NOTE: Only one argument is supported at a time!");
        return true;
    }

    @Override
    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    @NotNull
    public ProjectEndpointService getProjectService() {
        return projectEndpointService;
    }

    @Override
    @NotNull
    public TaskEndpointService getTaskService() {
        return taskEndpointService;
    }

    @Override
    @NotNull
    public UserEndpointService getUserService() {
        return userEndpointService;
    }

    @Override
    @NotNull
    public AdminEndpointService getDataService() {
        return adminEndpointService;
    }

    @Override
    @NotNull
    public SessionEndpointService getSessionService() {
        return sessionEndpointService;
    }

}
