package ru.aushakov.tm.client.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.api.service.IAuthService;
import ru.aushakov.tm.client.api.service.IPropertyService;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.ConfigProperty;
import ru.aushakov.tm.client.exception.general.NoUserLoggedInException;

import java.util.Optional;

public final class AuthService implements IAuthService {

    @Nullable
    private Session currentSession;

    @NotNull
    private final String secret;

    @NotNull
    private final Integer iterationNum;

    public AuthService(
            @NotNull final IPropertyService propertyService
    ) {
        this.secret = propertyService.getProperty(ConfigProperty.PASSWORD_SECRET);
        this.iterationNum = propertyService.getIntProperty(ConfigProperty.PASSWORD_ITERATION);
    }

    @Override
    public boolean isUserAuthenticated() {
        return (currentSession != null);
    }

    @Override
    @Nullable
    public Session getSession() {
        return currentSession;
    }

    @Override
    public void setSession(@Nullable final Session session) {
        this.currentSession = session;
    }

    @Override
    @NotNull
    public String getUserId() {
        Optional.ofNullable(currentSession).orElseThrow(NoUserLoggedInException::new);
        return currentSession.getUserId();
    }

}
