package ru.aushakov.tm.client.exception.general;

public class WrongCredentialsException extends RuntimeException {

    public WrongCredentialsException() {
        super("Wrong credentials, login failed!");
    }

}
